﻿using Domain;
using MediatR;
using Persistence;

namespace Application.Activities.Queries;

public class GetActivityDetails
{
    public class Query : IRequest<Activity>
    {
        public required string Id { get; set; }
    }

    public class Handler(AppDbContext dbContext) : IRequestHandler<Query, Activity>
    {
        public async Task<Activity> Handle(Query request, CancellationToken token)
        {
            var activity = await dbContext.Activities.FindAsync([request.Id], token);
            
            if (activity is null) throw new Exception("Activity not found");

            return activity;
        }
    }
}