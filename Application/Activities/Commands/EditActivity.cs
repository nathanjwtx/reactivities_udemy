﻿using AutoMapper;
using Domain;
using MediatR;
using Persistence;

namespace Application.Activities.Commands;

public class EditActivity
{
    public class Command : IRequest
    {
        public required Activity Activity { get; init; }
    }

    public class Handler(AppDbContext dbContext, IMapper mapper) : IRequestHandler<Command>
    {
        public async Task Handle(Command request, CancellationToken token)
        {
            var activity = await dbContext.Activities.FindAsync([request.Activity.Id], token) ?? 
                           throw new Exception("Cannot find activity");

            mapper.Map(request.Activity, activity);
            
            await dbContext.SaveChangesAsync(token);
        }
    }
}