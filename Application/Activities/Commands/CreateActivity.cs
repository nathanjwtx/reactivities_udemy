﻿using Domain;
using MediatR;
using Persistence;

namespace Application.Activities.Commands;

public class CreateActivity
{
    public class Command : IRequest<string>
    {
        public required Activity Activity { get; init; }
    }

    public class Handler(AppDbContext dbContext) : IRequestHandler<Command, string>
    {
        public async Task<string> Handle(Command request, CancellationToken token)
        {
            dbContext.Add(request.Activity);
            await dbContext.SaveChangesAsync(token).ConfigureAwait(false);

            return request.Activity.Id;
        }
    }
}