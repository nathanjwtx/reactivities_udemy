﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Activities.Commands;
using Application.Activities.Queries;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace API.Controllers;

public class Activities(ILogger logger) : BaseController
{
    [HttpGet]
    public async Task<ActionResult<List<Activity>>> GetActivities()
    {
        logger.Information("All Activities request received...");

        return await Mediator.Send(new GetActivityList.Query());
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Activity>> GetActivity(string id)
    {
        try
        {
            return await Mediator.Send(new GetActivityDetails.Query{Id = id});
        }
        catch (Exception e)
        {
            logger.Error("Activity was not found: {ex}", e);
            return NotFound();
        }
    }

    [HttpPost]
    public async Task<ActionResult<string>> CreateActivity(Activity activity)
    {
        return await Mediator.Send(new CreateActivity.Command {Activity = activity});
    }

    [HttpPut]
    public async Task<ActionResult> EditActivity(Activity activity)
    {
        await Mediator.Send(new EditActivity.Command {Activity = activity});
        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeleteActivity(string id)
    {
        await Mediator.Send(new DeleteActivity.Command {Id = id});
        return Ok("Deleted");
    }
}